package co.simplon.promo16;

public class Card {
    private String symbol;
    private boolean isFlipped;

    
    public Card(String symbol) {
        this.symbol = symbol;
        this.isFlipped = false;
    }


    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(int index) {
        this.symbol = symbol;
    }


    public boolean isFlipped() {
        return isFlipped;
    }

    public void setFlipped() {
        this.isFlipped = !this.isFlipped;
    }
}
