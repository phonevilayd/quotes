package co.simplon.promo16;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    
        MemoryGame game = new MemoryGame();
        System.out.println("Choose two card number between 0 & " + (game.cardBoard.size() - 1) + " :");
        game.compare(scanner);

        System.out.println();

        System.out.println();
        scanner.close();
    }

}
