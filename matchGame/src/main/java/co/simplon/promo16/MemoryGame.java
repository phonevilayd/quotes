package co.simplon.promo16;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MemoryGame {
    private Card first;
    private Card second;
    private int firstIndex;
    private int secondIndex;

    List<Card> cardBoard = new ArrayList<>(List.of(new Card("🐖"), new Card("🐃"), new Card("🦬"), new Card("🦌"),
            new Card("🐎"), new Card("🐆"), new Card("🐩"), new Card("🐒"),
            new Card("🐖"), new Card("🐃"), new Card("🦬"), new Card("🦌"), new Card("🐎"), new Card("🐆"),
            new Card("🐩"), new Card("🐒")));

    List<Boolean> flippedCards = new ArrayList<>();

    public void compare(Scanner scanner) {

        for (int i = 0; i < cardBoard.size(); i++) {
            flippedCards.add(false);

        }

        for (Boolean Card : flippedCards) {
            while (flippedCards.contains(false)) {

                try {
                    firstIndex = scanner.nextInt();
                    first = cardBoard.get(firstIndex);
                    System.out.println(first.getSymbol());
                    secondIndex = scanner.nextInt();
                    second = cardBoard.get(secondIndex);
                    System.out.println(second.getSymbol());

                } catch (IndexOutOfBoundsException e) {
                    // Trouver comment catch le fait que le nextInt est supérieur à
                    // La taille du tableau cardBoard afin de relancer un scanner nextInt
                    System.out.println("Error, please enter a number between 0 & " + (cardBoard.size() - 1) + " :|");
                }
                while (first == second) {
                    System.out.println("DON'T CHEAT :) please enter another number");
                    second = cardBoard.get(scanner.nextInt());
                }

                // faire une condition de vérification si la ou les cartes demandées
                // n'ont pas déjà été trouvées
                // si déjà trouvées faire un sysout le signalant et demander une nouvelle saisie
                // à comparer.

                if (first.isFlipped() || second.isFlipped()) {
                    System.out.println("Card already choosed, please pick another one :/");

                    // passer les valeurs du tableau flippedCards en true pour les index trouvés
                    // et faire un sysout signalant qu'elles matchent

                } else if (first.getSymbol() == second.getSymbol()) {

                    // System.out.println(flippedCards.get(firstIndex));
                    // System.out.println(flippedCards.get(secondIndex));
                    flippedCards.set(firstIndex, true);
                    flippedCards.set(secondIndex, true);

                    System.out.println("Matching cards :)");
                    // System.out.println(flippedCards.get(firstIndex));
                    // System.out.println(flippedCards.get(secondIndex));

                    // sinon si les cards ne correspondent pas, faire un sysout le signalant et
                    // demander
                    // de nouveau une nouvelle saisie
                } else {
                    System.out.println("Cards don't match, try again ;)");
                }

            }
            scanner.close();
        }

    }
}
