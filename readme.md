Matching memory game

On commence avec une liste de cartes qui contient des paires identiques.

Les cartes sont placées au hasard, face contre table.

Le joueur choisit deux cartes, les retournent, & si celles-ci correspondent, elles restent face retournée. Si elles ne correspondent pas, elles retournent dans leur position d'origine. Le jeu continue jusqu'à ce que les cartes soient toutes retournées.

Toutes les cartes face retournée sont indiquées par un numéro. 

Premièrement, on choisit un numéro de carte entre 0 & 15.
Le premier numéro fait apparaitre l'image correspondant à la carte.

Ensuite, on choisit toujours un numéro de carte, différent du premier, toujours entre 0 & 15.
Le deuxième numéro fait apparaître l'image correspondant à la carte.